# Kubernetes Nginx Example
> This is meant to be a simple reference point for deployment and service in kubernetes  
> This uses nginx due to its simple nature and easy to reference for container/pod deployments

## Files
---
`nginx_deploy.yaml` - config file to create a kubernetes deployment for nginx

`nginx_service.yaml` - config file to create a service to expose nginx deployment

## Running
---
> This will deploy an nginx app reachable one any nodehost on port 30080. (`<node_ip_address>`:30080)

### Create deployment
```
kubectl create -f nginx_deploy.yaml

# to view deployment after
kubectl get deployment
```

### Create Service
```
kubectl create -f nginx_service.yaml

# to view service after
kubectl get service
```

### Updating changes
```
# to apply any updates to the config
kubectl apply -f <config_file>.yaml

# for example if you update the deployment config
kubectl apply -f nginx_deploy.yaml

```


## Resources
---
- https://kubernetes.io/docs/concepts/services-networking/service/
- https://karthi-net.medium.com/kubernetes-tutorial-create-deployments-using-yaml-file-90ea901a2f74

